package spd.university.fp;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/*
Benchmark                              (count)  Mode  Cnt     Score     Error  Units
BookSortBenchmark.cachedClass               10  avgt   20    74,499 ±   1,039  ns/op
BookSortBenchmark.cachedClass              100  avgt   20   653,001 ±  44,435  ns/op
BookSortBenchmark.cachedClass             1000  avgt   20  7710,792 ± 763,745  ns/op
BookSortBenchmark.comparatorComparing       10  avgt   20    76,431 ±   0,447  ns/op
BookSortBenchmark.comparatorComparing      100  avgt   20   690,286 ±  19,940  ns/op
BookSortBenchmark.comparatorComparing     1000  avgt   20  7580,301 ± 201,467  ns/op
BookSortBenchmark.lambda                    10  avgt   20    70,075 ±   3,388  ns/op
BookSortBenchmark.lambda                   100  avgt   20   741,625 ±   6,303  ns/op
BookSortBenchmark.lambda                  1000  avgt   20  7328,153 ±  35,914  ns/op
BookSortBenchmark.newAnonymousClass         10  avgt   20    86,041 ±   2,306  ns/op
BookSortBenchmark.newAnonymousClass        100  avgt   20   780,785 ±  16,831  ns/op
BookSortBenchmark.newAnonymousClass       1000  avgt   20  8353,948 ± 306,665  ns/op
BookSortBenchmark.newClass                  10  avgt   20    69,063 ±   1,220  ns/op
BookSortBenchmark.newClass                 100  avgt   20   643,839 ±  39,562  ns/op
BookSortBenchmark.newClass                1000  avgt   20  7403,122 ± 236,879  ns/op
*/

@State(Scope.Thread)
public class BookSortBenchmark {

    @Param({"10", "100", "1000"})
    public int count;

    private BookStore bookStore = new BookStore();
    private List<List<Book>> books;

    private Comparator<Book> bookComparator = new Comparator<Book>() {
        @Override
        public int compare(Book o1, Book o2) {
            return o1.getAuthor().compareTo(o2.getAuthor());
        }
    };

    private static class BookComparator implements Comparator<Book> {

        @Override
        public int compare(Book o1, Book o2) {
            return o1.getAuthor().compareTo(o2.getAuthor());
        }
    }

    @Setup
    public void init() {
        books = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            books.add(Arrays.asList(
                    new Book("Author name", "title"),
                    new Book("Some author", "title")
            ));
        }
    }

    @Benchmark
    public void newAnonymousClass() {
        books.forEach(b -> b.sort(new Comparator<Book>() {
            @Override
            public int compare(Book o1, Book o2) {
                return o1.getAuthor().compareTo(o2.getAuthor());
            }
        }));
    }

    @Benchmark
    public void newClass() {
        books.forEach(b -> b.sort(new BookComparator()));
    }

    @Benchmark
    public void cachedClass() {
        books.forEach(b -> b.sort(bookComparator));
    }

    @Benchmark
    public void lambda() {
        books.forEach(b -> b.sort((o1, o2) -> o1.getAuthor().compareTo(o2.getAuthor())));
    }

    @Benchmark
    public void comparatorComparing() {
        books.forEach(b -> b.sort(Comparator.comparing(Book::getAuthor)));
    }
}
