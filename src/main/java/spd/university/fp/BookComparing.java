package spd.university.fp;

import java.util.Comparator;
import java.util.List;

public class BookComparing {

    public static void main(String[] args) {
        Comparator<Book> bookComparator = Comparator.comparing(Book::getAuthor)
                .thenComparing(Book::getTitle);

        final BookStore bookStore = new BookStore();
        final List<Book> books = bookStore.getAllBooks();
        books.sort(bookComparator);
        Utils.printBooks(books);
    }
}
