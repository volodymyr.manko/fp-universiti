package spd.university.fp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class BookStore {

    private final List<Book> books = Arrays.asList(
            new Book("Tolkien", "The Silmarillion"),
            new Book("Stephen King", "It"),
            new Book("Ray Bradbury", "Fahrenheit 451"),
            new Book("Isaac Asimov", "The Gods Themselves"),
            new Book("Stephen King", "The Shining"),
            new Book("Ray Bradbury", "The Martian Chronicles"),
            new Book("Tolkien", "The Hobbit"),
            new Book("Stephen King", "Carrie"),
            new Book("Ray Bradbury", "Dandelion Wine"),
            new Book("Isaac Asimov", "I, Robot"),
            new Book("Stephen King", "Stephen King"),
            new Book("Ray Bradbury", "Something Wicked This Way Comes"),
            new Book("Isaac Asimov", "Nightfall"),
            new Book("Stephen King", "Green Mile"),
            new Book("Tolkien", "The Lord of the Rings"),
            new Book("Ray Bradbury", "The Illustrated Man")
    );

    public List<Book> getAllBooks() {
        return new ArrayList<>(books);
    }

    public List<Book> search(Predicate<Book> condition) {
        List<Book> filteredBooks = new ArrayList<>();
        for (Book book : books) {
            if (condition.test(book)) {
                filteredBooks.add(book);
            }
        }
        return filteredBooks;
    }
}
