package spd.university.fp;

import java.util.Optional;

public class NullAndOptional {

    public static void main(String[] args) {

    }

    private static void nullExample() {
        final Person person = getNullablePerson();
        if (person != null) {
            Car nullableCar = getNullableCar(person.getFavoriteCar());
            if (nullableCar != null) {
                String price = nullableCar.getPrice();
                System.out.println(price);
            }
        }
    }

    private static void optionalExample() {
        getOptionalPerson()
                .map(Person::getFavoriteCar)
                .flatMap(NullAndOptional::getOptionalCar)
                .map(Car::getPrice)
                .ifPresent(System.out::println);
    }


    private static Person getNullablePerson() {
        return new Person();
    }

    private static Optional<Person> getOptionalPerson() {
        return Optional.of(new Person());
    }

    private static Car getNullableCar(String carModel) {
        return new Car();
    }

    private static Optional<Car> getOptionalCar(String carModel) {
        String s = null;
        Optional.ofNullable(s);
        return Optional.empty();//Optional.of(new Car());
    }
}

class Person {

    private String favoriteCar = "Lada kalina";

    public String getFavoriteCar() {
        return favoriteCar;
    }

    @Override
    public String toString() {
        return "Person{" +
                "favoriteCar='" + favoriteCar + '\'' +
                '}';
    }
}

class Car {

    public String getPrice() {
        return "3 500 $";
    }
}

class NotFoundException extends RuntimeException {

}