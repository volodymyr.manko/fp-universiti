package spd.university.fp;

import java.util.List;
import java.util.function.Predicate;

public class Program {
    public static void main(String[] args) {
        Predicate<Book> isAuthorRay = book -> book.getAuthor().startsWith("Ray");
        Predicate<Book> titleStartsThe = book -> book.getTitle().startsWith("The");


        List<Book> books = new BookStore().search(titleStartsThe.and(isAuthorRay));
        Utils.printBooks(books);
    }
}