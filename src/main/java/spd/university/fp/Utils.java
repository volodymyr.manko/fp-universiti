package spd.university.fp;

import java.util.List;

public interface Utils {

    static void printBooks(List<Book> books) {
        books.forEach(book -> System.out.println(String.format("author = %s, title = %s", book.getAuthor(), book.getTitle())));
    }
}
