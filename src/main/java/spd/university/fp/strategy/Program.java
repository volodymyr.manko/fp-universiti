package spd.university.fp.strategy;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class Program {

    public static void main(String[] args) {
        List<String> params = Arrays.asList("some string", "some@email.com");

        printValidationResults(params, ValidationRules::isNotEmpty);
    }

    private static void printValidationResults(List<String> params, Predicate<String> validationRule) {
        final Validator validator = new Validator(validationRule);
        for (String param : params) {
            if (validator.isValid(param)) {
                System.out.println(String.format("Great! '%s' is valid param.", param));
            } else {
                System.out.println(String.format("Oops! Looks like '%s' is invalid param", param));
            }
        }
    }
}
