package spd.university.fp.strategy;

public class ValidationRules {

    public static boolean isNotEmpty(String param) {
        return !param.isEmpty();
    }

    public static boolean isEmailValid(String param) {
        return param.contains("@");
    }
}
