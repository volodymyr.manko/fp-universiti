package spd.university.fp.strategy;

import java.util.function.Predicate;

public class Validator {

    private final Predicate<String> validationRule;

    public Validator(Predicate<String> validationRule) {
        this.validationRule = validationRule;
    }

    public boolean isValid(String param) {
        return validationRule.test(param);
    }
}
