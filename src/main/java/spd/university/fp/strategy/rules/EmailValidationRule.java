package spd.university.fp.strategy.rules;

public class EmailValidationRule implements ValidationRule {
    @Override
    public boolean isValid(String param) {
        return param.contains("@");
    }
}
