package spd.university.fp.strategy.rules;

public class NotEmptyValidationRule implements ValidationRule {
    @Override
    public boolean isValid(String param) {
        return !param.isEmpty();
    }
}
