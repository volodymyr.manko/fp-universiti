package spd.university.fp.strategy.rules;

@FunctionalInterface
public interface ValidationRule {

    boolean isValid(String param);
}
